package kousin.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class kousinServlet
 */
@WebServlet("/kousinServlet")
public class kousinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public kousinServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User userList = userDao.user(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/kousin.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao dao = new UserDao();

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("pass");
		String name = request.getParameter("user");
		String birthDate = request.getParameter("birthday");

		//ここの（）はkousin.jspの各nameを受け取っているのでUser.javaからではない。

		try {
			String pas = dao.pass(password);

			if (!(password.equals(password2)) || name.equals("") || birthDate.equals("")) {

				/** 入力エラーの場合 **/

				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/touroku.jsp");
				dispatcher.forward(request, response);
				return;

			} else if ((password.equals("") && password2.equals(""))) {

				/** 更新成功 **/

				// セッションにユーザの情報をセット
				dao.UPDATEsub(loginId, name, birthDate, null);

				HttpSession session = request.getSession();
				session.setAttribute("update", loginId);

				// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("UserListServlet");

			} else {
				/** 更新成功 **/

				// セッションにユーザの情報をセット
				dao.UPDATEMain(loginId, name, birthDate, pas, null);

				HttpSession session = request.getSession();
				session.setAttribute("update", loginId);

				// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("UserListServlet");

			}
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();

		}

	}

}
