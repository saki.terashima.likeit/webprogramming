package touroku.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class tourokuServlet
 */
@WebServlet("/tourokuServlet")
public class tourokuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public tourokuServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/touroku.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao dao = new UserDao();

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("pass");
		String name = request.getParameter("user");
		String birthDate = request.getParameter("birthday");

		//ここの（）はtouroku.jspの各nameを受け取っているのでUser.javaからではない。

		try {
			String pas = dao.pass(password);

			//入力項目が未記入の場合のインスタンス
			boolean isLogin = (loginId.equals("") || password.equals("") || name.equals("") || birthDate.equals(""));

			//ログインIDが登録されているかどうか
			User login = dao.userID(loginId);
			boolean logid = (login != null);

			//パスワードと確認用の比較
			boolean pass = (password.equals(password2));

			//↑のboolean達はif文でまとめたほうがわかりやすいかも

			if (isLogin || logid || !pass) {

				/** テーブルに該当のデータが見つかった場合 **/

				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/touroku.jsp");
				dispatcher.forward(request, response);
				return;

			} else {
				/** テーブルに該当のデータが見つからなかった場合 **/

				// セッションにユーザの情報をセット
				dao.InsertMain(loginId, name, birthDate, pas, null, null);

				// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("UserListServlet");

			}
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
