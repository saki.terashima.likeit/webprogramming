package sakujo.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class sakujoServlet
 */
@WebServlet("/sakujoServlet")
public class sakujoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public sakujoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));


		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User userList = userDao.user(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);



		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sakujo.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 // リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao dao = new UserDao();

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		   //ここの（）はsakujo.jspの各nameを受け取っているのでUser.javaからではない。


		if (id != null){
			dao.DELETEMain(Integer.parseInt(id));
			//String型のidをint型に変える
		}


		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");
		




	}

}
