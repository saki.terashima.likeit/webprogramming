<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
</head>
<body>
	<form method="post" action="loginServlet">
		<div align="center">
			<h1>ログイン画面</h1>
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">
					<font color="#ff0000"> ${errMsg} </font>
				</div>
			</c:if>


			<p>
				ログインID<input name="loginId">
			</p>
			<p>
				パスワード<input type="password" name="password">
			</p>
			<p>
				<input type=submit value="ログイン">
			</p>
	</form>
	</div>
</body>
</html>
