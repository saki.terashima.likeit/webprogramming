<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="sidebar" href="user.css">
<title>ユーザー一覧</title>
</head>
<body>
	<div align="center">
		<form method="post" action="UserListServlet">
			<nav>
				<ul>
					<li><font color="#00bfff">${userInfo.name }さん</font></li>
					<li><div align="right">
							<a href="loginServlet">ログアウト</a>
						</div></li>
				</ul>
			</nav>


			<h1>ユーザ一覧</h1>
			<p>
			<div align="right">
				<a href="tourokuServlet">新規登録</a>
			</div>

			<p>
				ログインID<input name="loginId">
			</p>
			<p>
				ユーザ名<input name="user">
			</p>
			<p>
				生年月日<input type="date" name="birthday">~<input type="date"
					name="birthday2">
			</p>


			<div align="right">
				<input type=submit value="検索">
			</div>
		</form>
	</div>

	<table>
		<tr>
			<th>ログインID</th>
			<th>ユーザー名</th>
			<th>生年月日</th>
			<th></th>
		</tr>

		<c:forEach var="user" items="${userList}">
			<%-- c:forEachは繰り返し表示されるので一人だけのデータだけの時は使わない --%>


			<tr>

				<td>${user.loginId}</td>
				<td>${user.name}</td>
				<td>${user.birthDate}</td>
				<td>
					<p>
						<a class="btn btn-primary" href="syousaiServlet?id=${user.id}">詳細</a>

						<c:if
							test="${userInfo.name == user.name or userInfo.name == '管理者' }">
							<%-- ログインした人のみ表示 --%>
							<a class="btn btn-success" href="kousinServlet?id=${user.id}">更新</a>
						</c:if>

						<c:if test="${userInfo.name == '管理者'}">
							<%-- 管理者の人以外表示しない --%>
							<a class="btn btn-danger" href="sakujoServlet?id=${user.id}">削除</a>
						</c:if>
					</p>
				</td>
			</tr>

		</c:forEach>




	</table>

</body>
</html>
