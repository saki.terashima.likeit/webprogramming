<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="user.css">
	<title>ユーザ情報詳細参照</title>
</head>
<body>
    <div align="center">
      <%--値をどこにも渡さないので<form>は書かなくてよい --%>
           <nav >
           <ul >
               <li><font color="#00bfff">${userInfo.name }さん</font></li>
               <li><div align = "right"><a href = "loginServlet">ログアウト</a></div></li>
           </ul>
        </nav>


            <h1 >ユーザ情報詳細参照</h1>

         <div align = "center">

             <p>ログインID ${userList.loginId}</p>
             <p>ユーザー名 ${userList.name}</p>
             <p>生年月日 ${userList.birthDate}</p>
             <p>登録日時 ${userList.createDate}</p>
             <p>更新日時 ${userList.updateDate}</p>

                    <%-- syousaiServletのuserListからデータを持ってくる --%>

        </div>
            <p><div align = "left"><a href = "UserListServlet">戻る</a></div>
        </form>
    </div>
</body>
</html>
