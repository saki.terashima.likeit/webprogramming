<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="user.css">
	<title>ユーザ情報更新</title>
</head>
<body>
    <div align="center">
    <form method="post" action="kousinServlet">
           <nav >
           <ul >
               <li><font color="#00bfff">${userInfo.name }さん</font></li>
               <li><div align = "right"><a href = "loginServlet">ログアウト</a></div></li>
           </ul>
        </nav>


            <h1>ユーザ情報更新</h1>
               <div align="center">
                  <c:if test="${errMsg != null}" >
                    <div class="alert alert-danger" role="alert"><font color="#ff0000">
                    ${errMsg}
                    </font>
                    </div>
                  </c:if>
                </div>
              
            <p>ログインID <input name="loginId" value="${userList.loginId}" readonly></p>
            <p>パスワード<input type="password" name = "password"></p>
            <p>パスワード（確認）<input type = "password" name = "pass"></p>
            <p>ユーザ名<input name = "user" value="${userList.name}"></p>
            <p>生年月日<input name = "birthday" value="${userList.birthDate}"></p>
            <p><input type=submit value="更新"></p>
            <p><div align = "left"><a href = "UserListServlet">戻る</a></div>
        </form>
    </div>

</body>
</html>
