<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="user.css">
	<title>ユーザ削除確認</title>
</head>
<body>
    <div align="center">
    <form method="post" action="sakujoServlet">

           <nav >
           <ul >
               <li><font color="#00bfff">${userInfo.name }さん</font></li>
               <li><div align = "right"><a href = "loginServlet">ログアウト</a></div></li>
           </ul>
        </nav>


         <h1>ユーザ削除確認</h1>

         <div align = "center">

         <p>ログインID:${userList.loginId}</p>
         <input type="hidden" name="id" value="${userList.id}">
              <%-- inputのtype=hiddenでデータを見えなくさせて、name=引数の名前、value=何のデータを渡すか --%>

         <p>を本当に削除してよろしいでしょうか。</p>
         <p>
             <button type ="button"  name = "no" onclick = "history.back()">キャンセル</button>
             <button  type = "submit"  name = "ok">OK</button>
         </p>
         </div>

        </form>
    </div>

</body>
</html>
