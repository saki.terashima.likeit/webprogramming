<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="user.css">
	<title>ユーザ新規登録</title>
</head>
<body>
    <div align="center">
    <form method="post" action="tourokuServlet">
           <nav >
           <ul >
               <li><font color="#00bfff">${userInfo.name }さん</font></li>
               <li><div align = "right"><a href = "loginServlet">ログアウト</a></div></li>
           </ul>
        </nav>


            <h1>ユーザ新規登録</h1>
                <div align="center">
                  <c:if test="${errMsg != null}" >
                    <div class="alert alert-danger" role="alert"><font color="#ff0000">
                    ${errMsg}
                    </font>
                    </div>
                  </c:if>
                </div>
    
            
            <p>ログインID<input name = "loginId"></p>
            <p>パスワード<input type="password" name = "password"></p>
            <p>パスワード（確認）<input type = "password" name = "pass"></p>
            <p>ユーザ名<input name = "user"></p>
            <p>生年月日<input type = "date" name = "birthday"></p>
            <p><input type=submit value="登録"></p>
            <p><div align = "left"><a href = "UserListServlet">戻る</a></div>
        </form>
    </div>
</body>
</html>
